import { Component, OnInit } from '@angular/core';
import { CaseResult } from '../models/case-result.model';
import { Result } from '../models/result.model';
import { AppService } from '../service/app.service';

@Component({
  selector: 'app-result-section',
  templateUrl: './result-section.component.html',
  styleUrls: ['./result-section.component.scss']
})
export class ResultSectionComponent implements OnInit {
  results: Array<Result> = [];
  showResult = true;
  selectedResult: Result;
  constructor(private appService: AppService) { }

  ngOnInit() {
    this.results = this.appService.results;
    this.selectedResult =  this.results[0];
    this.appService.showDetail.subscribe((value)=>{
      this.selectedResult = value;
    });
    this.appService.resize.subscribe(()=>{
      this.showResult = !this.showResult;
    });
  }


}
