
import { of } from 'rxjs';
import { ResultSectionComponent } from './result-section.component';

describe('ResultSectionComponent', () => {
  let component: ResultSectionComponent;
  const appService = { results: [],showDetail: of({test: 1}),resize: of()} as any;
   beforeEach(()=>{
    component = new ResultSectionComponent(appService);
   });

   describe('ngOnInit method',()=>{
     beforeEach(()=>{
      component.ngOnInit();
     });
    it('should set selectedResult value',()=>{
      expect(component.selectedResult).toEqual({test:1} as any);
    });
    it('should set showResult value',()=>{
      expect(component.selectedResult).toBeDefined();
    })
   });
});
