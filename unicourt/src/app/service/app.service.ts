import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Result } from "../models/result.model";
import { SearchParam } from "../models/search-param.model";

@Injectable({
    providedIn: 'root',
})
export class AppService {
    token = "6044fbb926adfd33c9f74dca6b8048d353fc52f4";
    results: Array<Result> = [];
    showDetail: EventEmitter<Result> = new EventEmitter<Result>();
    resize: EventEmitter<void> = new EventEmitter<void>();
    constructor(private httpClient: HttpClient){}

    searchCases(searchParam: SearchParam):Observable<any>{
        return this.httpClient.post(`https://api.unicourt.com/rest/v1/search/?token=${this.token}`,searchParam);
    }

}