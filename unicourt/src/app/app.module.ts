import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchSectionComponent } from './search-section/search-section.component';
import { ResultSectionComponent } from './result-section/result-section.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppService } from './service/app.service';
import { AuthInterceptor } from './service/auth-interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CaseCardComponent } from './case-card/case-card.component';
import { CaseDetailComponent } from './case-detail/case-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchSectionComponent,
    ResultSectionComponent,
    CaseCardComponent,
    CaseDetailComponent
  ],
  imports: [ 
    ReactiveFormsModule,
    BrowserModule,  
    AppRoutingModule,
    HttpClientModule,
    
  ],
  providers: [AppService,
],
  bootstrap: [AppComponent]
})
export class AppModule { }
