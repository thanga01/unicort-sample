
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  beforeEach(()=>{
    component = new AppComponent();
  })
  it('should set title',()=>{
    expect(component.title).toEqual('unicourt');
  });
});
