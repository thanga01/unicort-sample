import { CaseDetailComponent } from './case-detail.component';

describe('CaseDetailComponent', () => {
  let component: CaseDetailComponent;
  const appService = {resize: {emit: ()=>{}}} as any;
  beforeEach(()=>{
    component = new CaseDetailComponent(appService);
  });

  describe('resize method',()=>{
    it('should call resize emit method',()=>{
      spyOn(appService.resize,'emit');
      component.resize();
      expect(appService.resize.emit).toHaveBeenCalled();
    });
  });
});
