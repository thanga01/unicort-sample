import { Component, Input } from '@angular/core';
import { Result } from '../models/result.model';
import { AppService } from '../service/app.service';

@Component({
  selector: 'app-case-detail',
  templateUrl: './case-detail.component.html',
  styleUrls: ['./case-detail.component.scss']
})
export class CaseDetailComponent {
  expanded = false;
  @Input() result: Result;
  constructor(private appService: AppService) { }

  resize(): void{
    this.expanded = !this.expanded;
    this.appService.resize.emit();
  }

  get caseDate(): Date{
    return new Date(this.result.case.created_date.split(' ')[0]);
  }
}
