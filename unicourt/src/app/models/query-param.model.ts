export interface QueryParam {
    search_terms: Array<string>;
}