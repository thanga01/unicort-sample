import { QueryParam } from "./query-param.model";

export interface SearchParam {
    query: Array<QueryParam>;
    page: number;
    sort_by: string;
    filters: Array<string>;
}