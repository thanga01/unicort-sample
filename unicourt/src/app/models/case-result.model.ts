import { Result } from "./result.model";

export interface CaseResult {
    result: Array<Result>;
}