export interface Case {
    case_type: string;
    case_type_category: string;
    case_type_group: string;
    case_status_categroy: string;
    case_status_name: string;
    courthouse: string;
    state: string;
    judisdication;
    last_updated_data: string;
    last_update_changes_found: string;
    created_date: string;
    case_name: string;
    filing_date: string;
    case_id: string;
    case_nummber: string;
    docket: string;
}