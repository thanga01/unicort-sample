import { CaseResult } from "./case-result.model";

export interface CaseResponse {
   error: boolean;
   message: string;
   data: CaseResult;
}