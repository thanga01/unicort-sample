import { Case } from "./case.model";
import { Party } from "./party.model";

export interface Result {
    case: Case;
    attorneys: Array<string>;
    parties: Array<Party>;
    judges: Array<string>;
}