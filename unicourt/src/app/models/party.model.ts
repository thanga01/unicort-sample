export interface Party {
    representation_type: string;
    party_type: string;
    fullname: string;
    entity_type: string;
}