
import { of } from 'rxjs';
import { SearchSectionComponent } from './search-section.component';

describe('SearchSectionComponent', () => {
  let component: SearchSectionComponent;
  const appServiceMock = {getCases: ()=>{},results: null,searchCases: ()=>{}} as any;
  const routerMock = {navigate: ()=>{}} as any;
  beforeEach(()=>{
    component = new SearchSectionComponent(appServiceMock,routerMock);
  });

  describe("getVal method",()=>{
    beforeEach(()=>{
      spyOn(appServiceMock,'searchCases').and.returnValue(of({data: {result: 1}}));
      spyOn(routerMock,'navigate');
      component.getVal();
    });
    it('should set appService results',()=>{
      expect(appServiceMock.results).toEqual(1);
    });
    it('should call router > navigate method',()=>{
      expect(routerMock.navigate).toHaveBeenCalledWith(['result']);
    });
  });
});
