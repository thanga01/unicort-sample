import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CaseResponse } from '../models/case-response.model';
import { SearchParam } from '../models/search-param.model';
import { AppService } from '../service/app.service';

@Component({
  selector: 'app-search-section',
  templateUrl: './search-section.component.html',
  styleUrls: ['./search-section.component.scss']
})
export class SearchSectionComponent  {
  searchValue = new FormControl('');
  constructor(private appService: AppService,private router: Router) { }

 getVal(): void{
  const searchParam: SearchParam = {
    query: [
      {
        search_terms: [
          this.searchValue.value
        ]
      }
    ],
    page: 1,
    sort_by: "Filing Date",
    filters: []
  };
   this.appService.searchCases(searchParam)
    .subscribe((value: CaseResponse)=>{
      this.appService.results = value.data.result;
      this.router.navigate(['result']);
   },(error)=>{
     alert(error.message);
   });
 }
}
