import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultSectionComponent } from './result-section/result-section.component';
import { SearchSectionComponent } from './search-section/search-section.component';

const routes: Routes = [
  {
    path: '',
    component: SearchSectionComponent
  },
  {
    path: 'result',
    component: ResultSectionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
