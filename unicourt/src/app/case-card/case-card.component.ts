import { Component, Input, OnInit } from '@angular/core';
import { Case } from '../models/case.model';
import { Result } from '../models/result.model';
import { AppService } from '../service/app.service';

@Component({
  selector: 'app-case-card',
  templateUrl: './case-card.component.html',
  styleUrls: ['./case-card.component.scss']
})
export class CaseCardComponent  {
  @Input() result: Result;
  constructor(private appService: AppService) { }

  showDetail(result): void{
    this.appService.showDetail.emit(result);
  }

}
