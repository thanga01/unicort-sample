import { CaseCardComponent } from './case-card.component';

describe('CaseCardComponent', () => {
  let component: CaseCardComponent;
  const appService = {showDetail: {emit: ()=>{}}} as any;
  beforeEach(()=>{
    component = new CaseCardComponent(appService);
  });

  describe('showDetail method',()=>{
    it('should call appService emit method',()=>{
      spyOn(appService.showDetail,'emit');
      component.showDetail({test: 1});
      expect(appService.showDetail.emit).toHaveBeenCalledWith({test: 1});
    });
  });
});
